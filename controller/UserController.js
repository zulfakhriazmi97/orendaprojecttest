'use strict';
const db = require('../config/db');
const Op = db.Sequelize.Op;
db.authenticate().then(() => console.log("berhasil koneksi dengan database"));
const User = require("../models/User");

exports.index = async (req, res) => {
    try {
        const getAllUser = await User.findAll({});

        res.json(getAllUser);
    } catch (err) {
        console.log(err.message);
        res.status(500).send("server error")
    }
};

exports.register = async (req, res) => {
    try {
        const { username, email, password } = req.body;

        const newUser = new User({
            username, email, password
        })

        await newUser.save();

        const data = {
            'status': 204 + ' No Content',
        };
        res.json(data);
    } catch (err) {
        console.log(err.message);
        res.status(500).send("server error")
    }
}
