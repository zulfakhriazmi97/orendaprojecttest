'use strict';
const db = require('../config/db');
const Op = db.Sequelize.Op;
const User = require("../models/User");
const Task = require("../models/Task");
db.authenticate().then(() => console.log("berhasil koneksi dengan database"));

exports.assign = async (req, res) => {
    try {
        const { username, tasks } = req.body;
        const getUser = await User.findOne({
            where: {
                username: {
                    [Op.like]: '%'+username+'%'
                }
            },
            attributes: ['id']
        });

        let id_user = getUser.id;
        let task = JSON.stringify(tasks);

        const newTask = new Task({
            id_user, task
        })

        await newTask.save();

        const data = {
            'status': 204 + ' No Content',
        };
        res.json(data);
    } catch (err) {
        console.log(err.message);
        res.status(500).send("server error")
    }
}

exports.unassign = async (req, res) => {
    try {
        const { username, tasks } = req.body;
        const getUser = await User.findOne({
            where: {
                username: {
                    [Op.like]: '%' + username + '%'
                }
            },
            attributes: ['id']
        });

        const id_user = getUser.id;
        const getTask = await Task.findOne({
            where: {
                id_user: id_user
            },
            attributes: ['task']
        });

        let taskParse = JSON.parse(getTask.task);
        let taskRemove = taskParse.filter(function (e) { return e !== tasks });
        let task = JSON.stringify(taskRemove);

        const updateTask = await Task.update({
            id_user, task
        }, { where: { id_user: id_user } });

        await updateTask;

        const data = {
            'status': 204 + ' No Content',
        };

        res.json(data);
    } catch (err) {
        console.log(err.message);
        res.status(500).send("server error")
    }
}

exports.commontask = async (req, res) => {
    try {
        const { username } = req.body;
        const waitFor = (ms) => new Promise(r => setTimeout(r, ms))
        const asyncForEach = async (array, callback) => {
            for (let index = 0; index < array.length; index++) {
                await callback(array[index], index, array)
            }
        }

        const start = async () => {
            let _data = [];
            await asyncForEach(username, async (element) => {
                await waitFor(50)
                let result = [];
                const getUser = await User.findOne({
                    where: {
                        username: {
                            [Op.like]: '%' + element + '%'
                        }
                    },
                    attributes: ['id']
                });

                const getTask = await Task.findOne({
                    where: {
                        id_user: getUser.id
                    },
                    attributes: ['task']
                });

                let tasks = JSON.parse(getTask.task);
                tasks.forEach( (value, key) => {
                    const thisValue = result.find(x => x == value);
                    if(thisValue != value) {
                        result.push(value);
                    }
                });
                
                JSON.stringify(result);
                _data = {
                    'tasks': result,
                };
            });
            console.log('data >>>>', _data);
            res.json(_data);
        }        
        start()        
    } catch (err) {
        console.log(err.message);
        res.status(500).send("server error")
    }
}


