# OrendaProjectTest

Before running server, Install some application below:
1. NodeJS
2. MySQL (you can install xampp)

Running this Project: <br>
use > nodemon app.js <br>
server: localhost:4500

Database Name: testOrenda

Database Structure: <br>
User : username, email, password <br>
Task : id_user, task

Running Test (using Postman): <br>
No.1 : use method POST and write url: localhost:4500/api/register with body: username, email, password <br>
No.2 : use method POST and write url: localhost:4500/api/assign with body: username, tasks <br>
No.3 : use method POST and write url: localhost:4500/api/unassign with body: username, tasks (can be more than one task) <br>
No.4 : use method POST and write url: localhost:4500/api/unassign with body: username (can be more than one username)



