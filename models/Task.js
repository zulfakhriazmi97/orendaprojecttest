const Sequelize = require('sequelize');
const db = require("../config/db");

const Task = db.define(
    "task",
    {
        id_user: { type: Sequelize.INTEGER },
        task: { type: Sequelize.TEXT }
    },
    {
        freezeTableName: true
    }
);

module.exports = Task;