'use strict';

module.exports = function (app, express) {
    app.get('/', (req, res) => res.send("respon nodejs berhasil"));
    app.use(express.urlencoded({ extended: true }));

    app.listen(4500, () => console.log('Aplikasi Berjalan di Port 4500'));
    const user = require('./controller/UserController');
    const task = require('./controller/TaskController.js');

    app.route('/api/register').post(user.register);
    app.route('/getuser').get(user.index);
    app.route('/api/assign').post(task.assign);
    app.route('/api/unassign').post(task.unassign);
    app.route('/api/tasks/common').post(task.commontask);
}